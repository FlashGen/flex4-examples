package com.pixadecimal.viewstates
{
	import mx.core.DeferredInstanceFromFunction;
	import mx.states.AddItems;
	import mx.states.State;
	
	import spark.components.Group;
	import spark.components.Panel;
	
	public class DIFFExample extends Group
	{
		
		private static const DEFAULT_WIDTH 		:uint = 200;
		private static const DEFAULT_HEIGHT		:uint = 262;
		
		private var _stateNames		:Array = ["normal", "myFirstState", "mySecondState"];
		private var _count			:int;
		
		public function DIFFExample()
		{
			super();
		}		override protected function createChildren():void
		{
			super.createChildren();
			createStates();	
		}
		
		override protected function commitProperties():void
		{
			super.commitProperties();
		}
		
		override protected function measure():void
		{
			super.measure();
			
			this.measuredWidth = this.measuredMinWidth = DEFAULT_WIDTH;
			this.measuredHeight = this.measuredMinHeight = DEFAULT_HEIGHT;
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			
			this.setActualSize(unscaledWidth, unscaledHeight);
			
		}
		
		protected function createStates():void
		{
			
			for(var i:int = 0; i < _stateNames.length; ++i)
			{
				_count = i;
				
				var _state	:State = new State();
				_state.name = _stateNames[i];
				var _items	:AddItems = new AddItems();
				_items.itemsFactory = new DeferredInstanceFromFunction(createContent);
				_state.overrides.push(_items);
				states.push(_state);
			}		
		}
		
		protected function createContent():Panel
		{
			var _panel		:Panel = new Panel();
			_panel.percentWidth = 100;
			_panel.percentHeight = 100;
			_panel.title = "My Panel";
			return _panel;
		}
	}
}


