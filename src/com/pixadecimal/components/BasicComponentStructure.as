package com.pixadecimal.components
{
	import spark.components.supportClasses.SkinnableComponent;

	/**
	 * 
	 * @author Mike Jones
	 * @url http://www.pixadecimal.com
	 * @created 2010-09-25 22:05:09
	 * @version 1.0.0
	 * 
	 */		
	public class BasicComponentStructure extends SkinnableComponent
	{
		private static const DEFAULT_WIDTH 		:uint = 320;
		private static const DEFAULT_HEIGHT		:uint = 200;

		public function BasicComponentStructure()
		{
			super();
		}

		override protected function createChildren():void
		{
			super.createChildren();
		}
		
		override protected function commitProperties():void
		{
			super.commitProperties();
		}
		
		override protected function measure():void
		{
			super.measure();
			
			this.measuredWidth = this.measuredMinWidth = DEFAULT_WIDTH;
			this.measuredHeight = this.measuredMinHeight = DEFAULT_HEIGHT;
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			
			this.setActualSize(unscaledWidth, unscaledHeight);
		}
	}
}