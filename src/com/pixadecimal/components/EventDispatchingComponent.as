package com.pixadecimal.components
{

	import com.pixadecimal.events.ExampleEvent;
	
	import flash.events.MouseEvent;
	
	import mx.core.UIComponent;
	
	import spark.components.Button;

	[Event(name="defaultEvent", type="com.pixadecimal.events.ExampleEvent")]
	public class EventDispatchingComponent extends UIComponent
	{
		private static const DEFAULT_WIDTH 		:uint = 320;
		private static const DEFAULT_HEIGHT		:uint = 200;

		private var _submitBtn		:Button;
		private var _data			:Object;

		public function EventDispatchingComponent()
		{
			super();
		}

		override protected function createChildren():void
		{
			super.createChildren();
			_submitBtn = new Button();
			_submitBtn.label = "Submit";
			_submitBtn.setActualSize(100, 50);
			_submitBtn.addEventListener(MouseEvent.CLICK, createPayload);
			addChild(_submitBtn);
		}
		
		override protected function commitProperties():void
		{
			super.commitProperties();
		}
		
		override protected function measure():void
		{
			super.measure();
			
			this.measuredWidth = this.measuredMinWidth = DEFAULT_WIDTH;
			this.measuredHeight = this.measuredMinHeight = DEFAULT_HEIGHT;
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			
			this.setActualSize(unscaledWidth, unscaledHeight);
		}
		
		protected function createPayload(e:MouseEvent):void
		{
			_data = {name:"Event Component", typed:false, id:1};
			dispatchEvent(new ExampleEvent(ExampleEvent.DEFAULT_EVENT, _data));
		}
	}
}