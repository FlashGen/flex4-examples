package com.pixadecimal.effects.transitions
{
	import mx.core.DeferredInstanceFromFunction;
	import mx.effects.Parallel;
	import mx.graphics.SolidColor;
	import mx.states.AddItems;
	import mx.states.SetProperty;
	import mx.states.State;
	import mx.states.Transition;
	
	import spark.components.BorderContainer;
	import spark.components.Group;
	import spark.effects.Fade;
	import spark.effects.Move;
	import spark.primitives.Rect;
	
	/**
	 * 
	 * @author Mike Jones
	 * @url http://www.pixadecimal.com
	 * @created 2010-08-31 10:23:50
	 * @version 1.0.0
	 * 
	 */		
	public class TransitionComponentAS extends BorderContainer
	{
		//////////////////////////////////////////
		//			Static Constants 			//
		//////////////////////////////////////////
		private static const DEFAULT_WIDTH 		:uint = 320;
		private static const DEFAULT_HEIGHT		:uint = 200;
		
		//////////////////////////////////////////
		//   Private / Protected  Variables     //
		//////////////////////////////////////////
		private var _moveUp				:Move;
		private var _moveDown			:Move;
		private var _defaultTransition	:Transition;
		private var _panelTransition	:Transition;
		
		
		private var _defaultState		:State;
		private var _panelState			:State;	
		
		private var _defaultItems		:AddItems;
		private var _updatePanel		:SetProperty;
		private var _panel				:BorderContainer;
		
		private var _panelHeight		:int = 100;
		
		private var _mask				:Group;
		
		//////////////////////////////////////////
		//            	Constructor             //
		//////////////////////////////////////////
		public function TransitionComponentAS()
		{
			super();
			setStyle("backgroundColor", 0xcccccc);
			setStyle("borderVisible", false);
		}
		
		//////////////////////////////////////////
		//     Private / Protected  Methods     //
		//////////////////////////////////////////
		override protected function createChildren():void
		{
			super.createChildren();

			createMask();
			createPanel();
			createEffects();
			createStates();
			createTransitions();
		}
		
		override protected function measure():void
		{
			super.measure();
			
			this.measuredWidth = this.measuredMinWidth = DEFAULT_WIDTH;
			this.measuredHeight = this.measuredMinHeight = DEFAULT_HEIGHT;
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			
			this.setActualSize(unscaledWidth, unscaledHeight);
			
		}
		
		protected function createPanel():void
		{
			_panel = new BorderContainer();
			_panel.setStyle("backgroundColor", 0x333333);
			_panel.percentWidth = 100;
			_panel.height = _panelHeight;
			_panel.y = this.height + _panelHeight*2;
			addElement(_panel);
		}
		
		protected function createMask():void
		{
			var _rect		:Rect = new Rect();
			_rect.fill = new SolidColor(0xff0000);
			_rect.width = DEFAULT_WIDTH;
			_rect.height = DEFAULT_HEIGHT;
			
			_mask = new Group();
			_mask.addElement(_rect);
			_mask.width = DEFAULT_WIDTH;
			_mask.height = DEFAULT_HEIGHT;
			addElement(_mask);
			this.mask = _mask;
		}
		
		protected function createEffects():void
		{
			_moveUp = new Move();
			_moveUp.yBy = -_panelHeight;
			_moveUp.target = _panel;			

			_moveDown = new Move();
			_moveDown.yBy = _panelHeight;
			_moveDown.target = _panel;			
		}
		
		protected function createStates():void
		{
			_defaultState = new State();
			_defaultState.name = "normal";
			_defaultItems = new AddItems();
		
			_defaultState.overrides.push(_defaultItems);
			
			_panelState = new State();
			_panelState.name = "panelView"
			_panelState.basedOn = "normal"
				
			_updatePanel = new SetProperty();
			_updatePanel.target = _panel;
			_updatePanel.name = "y";
			_updatePanel.value = this.height + _panelHeight;
			
			_panelState.overrides.push(_updatePanel);
			
			states.push(_defaultState, _panelState);
		}
		
		protected function createTransitions():void
		{
			_defaultTransition = new Transition();
			_defaultTransition.fromState = "*";
			_defaultTransition.toState = "panelView";
			_defaultTransition.effect = _moveUp;
			_defaultTransition.autoReverse = true;
			transitions.push(_defaultTransition);

			_panelTransition = new Transition();
			_panelTransition.fromState = "panelView";
			_panelTransition.toState = "*";
			_panelTransition.effect = _moveDown;
			_panelTransition.autoReverse = true;
			transitions.push(_panelTransition);
		}
	}
}